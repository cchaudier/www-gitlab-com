# frozen_string_literal: true

require 'yaml'
require 'fileutils'
require 'rest-client'
require_relative 'milestones'

module ReleasePosts
  class Kickoff
    attr_accessor :version, :date, :release_post_manager, :tw_lead, :tech_advisor, :pmm_lead

    def initialize(row)
      @version = row["version"]
      @date = row["date"]
      @release_post_manager = row["manager"]
      @tw_lead = row["structural_check"]
      @tech_advisor = row["technical_advisor"]
      @pmm_lead = row["messaging"]
    end
  end
end
